# 播音（MIPI接口）

#### 样例介绍

本样例通过MIPI接口的耳机连接开发板，在运行应用时播放*.pcm音频文件的内容。

#### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Peripherals/Audio/MIPIAudio
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Peripherals/Audio/MIPIAudio
  ```

#### 执行准备

1. 以HwHiAiUser用户登录开发板。

2. 设置环境变量。

   ```
   # 配置程序编译依赖的头文件与库文件路径
   export DDK_PATH=/usr/local/Ascend/ascend-toolkit/latest 
   export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub
   ```

#### 样例运行

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。

2. 编译样例源码。

   依次执行如下命令执行编译：

   ```
   mkdir build
   cd build
   cmake .. -DCMAKE_C_COMPILER=gcc -DCMAKE_SKIP_RPATH=TRUE
   make
   ```

   在build目录下会生成可执行文件sample_audio。

3. 运行样例。

   播音命令示例如下，其中pcm文件需根据实际情况替换，文件格式为采样率48kHz，位宽为16bit的单声道音频：

   ```
   ./sample_audio play qzgy_48kHz_16bit_1chn.pcm
   ```

   **注意**：录音任务需要执行 ps aux|grep audio 查询进程号，然后手动 kill -12 [pid] 结束进程

#### 相关操作

获取学习文档，请单击[AscendCL C&C++](https://hiascend.com/document/redirect/CannCommunityCppAclQuick)，查看最新版本的AscendCL推理应用开发指南，在其中的”媒体数据处理（含图像/视频等）>音频获取&音频播放“章节了解接口调用流程。