# 图像显示（MIPI接口）

#### 样例介绍

本样例支持通过mipi接口连接显示器，在显示器上显示yuv420SP格式、800*400分辨率的图片。

#### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Peripherals/Display/MIPIDisplay
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Peripherals/Display/MIPIDisplay
  ```

#### 执行准备
1. 确认已更新dt.img（关闭drm的配置，打开vdp的配置）

2. 以HwHiAiUser用户登录开发板。

3. 设置环境变量。

   ```
   # 配置程序编译依赖的头文件与库文件路径
   export NPU_HOST_LIB=/usr/local/Ascend/ascend-toolkit/latest/runtime/lib64/
   export NPU_HOST_INC=/usr/local/Ascend/ascend-toolkit/latest/runtime/include/
   ```

#### 运行样例

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。

2. 编译样例源码。

   依次执行如下命令执行编译：

   ```
   mkdir build
   cd build
   cmake .. -DCMAKE_C_COMPILER=gcc -DCMAKE_SKIP_RPATH=TRUE
   make
   ```

   在build目录下会生成可执行文件raspberry_demo。

3. 运行样例。

   可执行文件的运行命令示例如下，需提前准备测试图片，与可执行文件raspberry_demo放置在同一目录下，图像格式参数要求如下，分辨率：800*400，颜色空间：yuv420SP格式。

   ```
   ./raspberry_demo car2.yuv
   ```

#### 相关操作

暂无。