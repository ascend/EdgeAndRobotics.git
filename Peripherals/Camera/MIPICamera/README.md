# Camera图像获取（MIPI接口）

#### 样例介绍

通过MIPI接口连接树莓派Camera与开发板，从树莓派Camera获取图像、并处理为YUV或RAW格式的图像。

本样例支持IMX219、IMX477两种型号的树莓派Camera，最多支持两个Camera。

#### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Peripherals/Camera/MIPICamera
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Peripherals/Camera/MIPICamera
  ```

#### 执行准备

1. 以HwHiAiUser用户登录开发板。

2. 设置环境变量。

   ```
   # 配置程序编译依赖的头文件与库文件路径
   export DDK_PATH=/usr/local/Ascend/ascend-toolkit/latest 
   export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub

   # 用于查找树莓派IMX219、IMX477的so库文件libsns_imx219.so、libsns_imx477.so
   export LD_LIBRARY_PATH=/lib64:$LD_LIBRARY_PATH
   ```

#### 样例运行

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。


2. 编译样例源码。

   依次执行如下命令编译源码：

   ```
   # 切换到样例根目录
   mkdir build
   cd build
   cmake .. -DCMAKE_C_COMPILER=gcc -DCMAKE_SKIP_RPATH=TRUE
   make
   ```

   在build目录下会生成可执行文件vi_l1_sample。

3. 运行样例。

   可执行文件的运行命令格式如下：

   - param1：1为输出yuv图，2为输出raw图；
   - param2：Camera数量，可配置为1或2；
   - param3：树莓派Camera型号，1为imx219，2为imx477。

   ```
   ./vi_l1_sample param1 param2 param3
   ```

   命令示例：

   ```
   ./vi_l1_sample 1 1 1
   ```


#### 相关操作

获取学习文档，请单击[AscendCL C&C++](https://hiascend.com/document/redirect/CannCommunityCppAclQuick)，查看最新版本的AscendCL推理应用开发指南，在其中的”媒体数据处理（含图像/视频等）>Camera场景视频数据获取和处理“章节了解接口调用流程。