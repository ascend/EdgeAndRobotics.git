#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include "acllite_dvpp_lite/ImageProc.h"
#include "acllite_dvpp_lite/VideoWrite.h"

using namespace std;
using namespace acllite;
int main()
{
    int32_t deviceId = 0;
    int32_t saveNum = 10;
    string filePath = "../data/test.yuv";
    string outFile = "../out/result.h264";
    uint32_t width = 1920;
    uint32_t height = 1080;

    AclLiteResource aclResource(deviceId);
    bool ret = aclResource.Init();
    CHECK_RET(ret, LOG_PRINT("[ERROR] InitACLResource failed."); return 1);

    VideoWrite testWriter(outFile, width, height);
    CHECK_RET(testWriter.IsOpened(), LOG_PRINT("[ERROR] open %s failed.", outFile.c_str()); return 1);

    void* image;
    void* imageInfoBuf;
    uint32_t imageInfoSize = YUV420SP_SIZE(width,height);
    CHECK_RET(ReadBinFile(filePath, image, imageInfoSize), LOG_PRINT("[ERROR] ReadBinFile failed."); return NULL);
    imageInfoBuf = CopyDataToDevice((void *)image, imageInfoSize);
    ImageData dst(SHARED_PTR_DEV_BUF(imageInfoBuf), imageInfoSize, width, height, PIXEL_FORMAT_YUV_SEMIPLANAR_420);

    LOG_PRINT("[INFO] Start to encode...");
    for(int i=0; i<saveNum; i++) {
	ret = testWriter.Write(dst);
        CHECK_RET(ret, LOG_PRINT("[ERROR] Write image to h264 failed."); return NULL);
    }
    testWriter.Release();
    LOG_PRINT("[INFO] Frame write end.");
    return 0;
}
