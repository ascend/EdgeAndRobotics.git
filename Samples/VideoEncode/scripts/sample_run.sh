#!/bin/bash
ScriptPath="$( cd "$(dirname "$BASH_SOURCE")" ; pwd -P )"

function main()
{
  echo "[INFO] The sample starts to run"
  running_command="./main"
  cd ${ScriptPath}/../out
  CurrentPath=`pwd`
  if [ -d out_pic ];then
    rm -rf out_pic
  fi
  mkdir out_pic
  ${running_command}
  if [ $? -ne 0 ];then
      echo "[INFO] The program runs failed"
  else
      echo "[INFO] The program runs successfully. Save the output image in the $CurrentPath/out_pic directory"
  fi
}
main
