# 手写数字识别模型训练&推理

#### 样例介绍

本样例使用MNIST数据集实现了手写数字识别体的训练，onnx推理，om推理全过程。

#### 版本配套表

   | 配套                                                         | 版本    | 环境准备指导                                                 |
   | :------------------------------------------------------------: | :-------: | :------------------------------------------------------------: |
   | 固件与驱动                                                   | 23.0.0   | [固件驱动安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0005.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit) |
   | CANN                                                         | 7.0.0 |[CANN软件包安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0011.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | Python                                                       | 3.9.2   |[Python安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0064.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit#ZH-CN_TOPIC_0000002017916412?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | 硬件设备型号                                                   | Orange Pi AIpro   | -                                                            |

#### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Samples/Mnist_For_Pytorch
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Samples/Mnist_For_Pytorch
  ```

#### 执行准备

- 本样例中的模型支持PyTorch2.1.0、torchvision1.16.0版本，请参考[安装PyTorch](https://www.hiascend.com/document/detail/zh/canncommercial/700/envdeployment/instg/instg_0046.html)章节安装PyTorch以及torch_npu插件。
  ```
  # torch_npu由于需要源码编译,速度可能较慢,本样例提供 python3.9,torch2.1版本的torch_npu whl包
  wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/torch_npu-2.1.0rc1-cp39-cp39-linux_aarch64.whl
  
  # 使用pip命令安装
  pip3 install torch_npu-2.1.0rc1-cp39-cp39-linux_aarch64.whl
  ```

- 本样例中的模型还依赖一些其它库（具体依赖哪些库，可查看本样例目录下的requirements.txt文件），可执行以下命令安装：

  ```
  pip3 install -r requirements.txt  # PyTorch2.1版本
  ```

- 配置离线推理所需的环境变量。

  ```
  # 配置程序编译依赖的头文件与库文件路径
  export DDK_PATH=/usr/local/Ascend/ascend-toolkit/latest 
  export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub
  ```

- 安装离线推理所需的ACLLite库。

  参考[ACLLite仓](https://gitee.com/ascend/ACLLite)安装ACLLite库。


#### 模型训练

1. 以HwHiAiUser用户登录开发板，切换到样例目录下。

2. 设置环境变量减小算子编译内存占用。
     ```
     export TE_PARALLEL_COMPILER=1
     export MAX_COMPILE_CORE_NUMBER=1
     ```
3. 运行训练脚本。

   ```
   python3 main.py
   ```

   成功运行训练脚本后，会自动下载Mnist数据集，数据集目录结构如下（如果用户网络状况不佳可以自行下载数据集放到指定目录dataset下）：

   ```
   ├── dataset
         ├──MNIST
              ├──raw
                    │──train-labels-idx1-ubyte.gz
                    │──train-labels-idx1-ubyte
                    │──train-images-idx3-ubyte.gz
                    │──train-images-idx3-ubyte
                    │──t10k-labels-idx1-ubyte.gz
                    │──t10k-labels-idx1-ubyte
                    │──t10k-images-idx3-ubyte.gz
                    │──t10k-images-idx3-ubyte
   ```

   训练完成后，权重文件保存在当前路径下，并输出模型训练精度和性能信息。

   此处展示单Device、batch_size=64的训练结果数据：

   |  NAME  | Acc@1 |  FPS  | Epochs | AMP_Type | Torch_Version |
   | :----: | :---: | :---: | :----: | :------: | :-----------: |
   | 1p-NPU | 97.89 | 17.24 |   10   |    O2    |      2.1      |

#### 在线推理

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。

2. 执行以下命令，将训练生成的mnist.pt转换mnist.onnx模型。

   ```
   python3 export.py
   ```

   mnist.onnx模型生成在当前路径下。

3. 执行以下命令，获取在线推理的测试图片。

   ```
   cd data
   wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/mnist/8.jpg
   ```

4. 执行在线推理。

   ```
   cd ../onnxInfer/
   python3 infer.py
   ```

   执行成功后，在屏幕上的关键提示信息示例如下：

   ```
   [image_path:data/8.jpg] [inferssession_time:1349 pictures/s] [output:8]
   ```

#### 离线推理

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。

2. 获取测试图片数据。

   ```
   cd omInfer/data
   wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/mnist/8.jpg
   ```

   **注：**若需更换测试图片，则需自行准备测试图片，并将测试图片放到omInfer/data目录下。

3. 获取PyTorch框架的ResNet50模型（\*.onnx），并转换为昇腾AI处理器能识别的模型（\*.om）。
   - 当设备内存**小于8G**时，可设置如下两个环境变量减少atc模型转换过程中使用的进程数，减小内存占用。
     ```
     export TE_PARALLEL_COMPILER=1
     export MAX_COMPILE_CORE_NUMBER=1
     ```
   - 为了方便下载，在这里直接给出原始模型下载及模型转换命令,可以直接拷贝执行。
     ```
     # 将在线推理时导出的mnist.onnx模型拷贝到model目录下
     cd ../model
     cp ../../mnist.onnx ./
   
     # 获取AIPP配置文件
     wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/mnist/ecs/aipp.cfg
   
     # 模型转换
     atc --model=mnist.onnx --framework=5 --insert_op_conf=aipp.cfg --output=mnist --soc_version=Ascend310B4
     ```

     atc命令中各参数的解释如下，详细约束说明请参见[《ATC模型转换指南》](https://hiascend.com/document/redirect/CannCommunityAtc)。

     - --model：转换前模型文件的路径。
     - --framework：原始框架类型。5表示ONNX。
     - --output：转换后模型文件的路径。请注意，记录保存该om模型文件的路径，后续开发应用时需要使用。
     - --input\_shape：模型输入数据的shape。
     - --soc\_version：昇腾AI处理器的版本。

4. 编译样例源码。

   执行以下命令编译样例源码。

   ```
   cd ../scripts 
   bash sample_build.sh
   ```

5. 运行样例。

   执行以下脚本运行样例：

   ```
   bash sample_run.sh
   ```

   执行成功后，在屏幕上的关键提示信息示例如下：

   ```
   [INFO] value[0.999993] output[8]
   ```

#### 相关操作