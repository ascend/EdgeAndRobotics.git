#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include "acllite_dvpp_lite/ImageProc.h"
#include "acllite_dvpp_lite/VideoRead.h"

using namespace std;
using namespace acllite;
int main()
{
    int32_t deviceId = 0;
    int32_t saveNum = 10;
    string videoPath = "../data/test.mp4";
    string outPath = "./out_pic/output";
    AclLiteResource aclResource(deviceId);
    bool ret = aclResource.Init();
    CHECK_RET(ret, LOG_PRINT("[ERROR] InitACLResource failed."); return 1);
    VideoRead cap(videoPath, deviceId);
    CHECK_RET(cap.IsOpened(), LOG_PRINT("[ERROR] Open test.mp4 failed."); return NULL);
    ImageData frame;
    LOG_PRINT("[INFO] Start to decode...");
    for(int i=0; i<saveNum; i++) {
	string outPic = outPath + to_string(i) + ".yuv";
        ret= cap.Read(frame);
        if(!ret){
            break;
        }
	SaveBinFile(outPic, frame.data.get(), frame.size);
    }
    LOG_PRINT("[INFO] Frame read end.");
    return 0;
}
