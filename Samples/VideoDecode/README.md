# VideoDecode视频解码样例

#### 样例介绍

该样例为使用ACLLite接口完成视频解码，主要功能为读取mp4文件的前十帧并解码为YUV图片。

#### 版本配套表

   | 配套                                                         | 版本    | 环境准备指导                                                 |
   | :------------------------------------------------------------: | :-------: | :------------------------------------------------------------: |
   | 固件与驱动                                                   | 23.0.0   | [固件驱动安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0005.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit) |
   | CANN                                                         | 7.0.0 |[CANN软件包安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0011.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | Python                                                       | 3.9.2   |[Python安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0064.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit#ZH-CN_TOPIC_0000002017916412?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | 硬件设备型号                                                   | Orange Pi AIpro   | -                                                            |

#### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Peripherals/DVPP/vdec
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Peripherals/DVPP/vdec
  ```

#### 执行准备

1. 以HwHiAiUser用户登录开发板。

2. 设置环境变量。

   ```
   # 配置程序编译依赖的头文件与库文件路径
   export DDK_PATH=/usr/local/Ascend/ascend-toolkit/latest 
   export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub
   ```

3. 安装ACLLite库。

   参考[Common安装指导]()安装ACLLite Common库。

   参考[DVPPLite安装指导]()安装DVPPLite库。

#### 运行样例

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。

2. 获取测试视频数据。

   请从以下链接获取该样例的测试视频，放在data目录下。

   ```
   cd data 
   wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/003_Atc_Models/yolov5s/test.mp4 --no-check-certificate
   ```

   **注：** 若需更换测试视频，则需自行准备测试视频，并将测试视频放到data目录下。

4. 编译样例源码。

   执行以下命令编译样例源码。

   ```
   cd ../scripts 
   bash sample_build.sh
   ```

5. 运行样例。

   执行以下脚本运行样例：

   ```
   bash sample_run.sh
   ```

   执行成功后，在out/out_pic目录下会生成十帧YUV图片。

#### 相关操作

- 获取更多样例，请单击[Link](https://gitee.com/ascend/samples/tree/master/inference/modelInference)。
- 获取在线视频课程，请单击[Link](https://www.hiascend.com/edu/courses?activeTab=%E5%BA%94%E7%94%A8%E5%BC%80%E5%8F%91)。
- 获取学习文档，请单击[AscendCL C&C++](https://hiascend.com/document/redirect/CannCommunityCppAclQuick)，查看最新版本的AscendCL推理应用开发指南。
