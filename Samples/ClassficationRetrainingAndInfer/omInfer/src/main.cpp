#include <cmath>
#include <dirent.h>
#include <string.h>
#include <map>
#include "acllite_dvpp_lite/ImageProc.h"
#include "acllite_om_execute/ModelProc.h"

using namespace std;
using namespace acllite;

int main()
{    
    vector<string> labels = { {"female"},{"male"}};
    AclLiteResource aclResource;
    bool ret = aclResource.Init();
    CHECK_RET(ret, LOG_PRINT("[ERROR] InitACLResource failed."); return 1);
    
    ImageProc imageProc;
    ModelProc modelProc;
    ret = modelProc.Load("../model/resnet18.om");
    CHECK_RET(ret, LOG_PRINT("[ERROR] load model Resnet18.om failed."); return 1);
    ImageData src = imageProc.Read("../data/8.jpg");
    CHECK_RET(src.size, LOG_PRINT("[ERROR] ImRead image failed."); return 1);
    
    ImageData dst;
    ImageSize dsize(224, 224);

    imageProc.Resize(src, dst, dsize);    
    ret = modelProc.CreateInput(static_cast<void *>(dst.data.get()), dst.size);
    CHECK_RET(ret, LOG_PRINT("[ERROR] Create model input failed."); return 1);
    vector<InferenceOutput> inferOutputs;
    ret = modelProc.Execute(inferOutputs);
    CHECK_RET(ret, LOG_PRINT("[ERROR] model execute failed."); return 1);

    uint32_t dataSize = inferOutputs[0].size;
    // get result from output data set
    float* outData = static_cast<float*>(inferOutputs[0].data.get());
    if (outData == nullptr) {
        LOG_PRINT("get result from output data set failed.");
        return 1;
    }
    int index = 0;
    float max = 0;
    for (uint32_t j = 0; j < dataSize / sizeof(float); ++j) {
        if (outData[j] > max){
            max = outData[j];
            index = j;
        }
    }
    LOG_PRINT("[INFO] value[%lf] output[%s]", outData[index] , labels[index].c_str());
    outData = nullptr;
    return 0;
}

