# AscendCL图片分类应用<a name="ZH-CN_TOPIC_0000001641068453"></a>
  
### 样例介绍

基于PyTorch框架的ResNet50模型，对\*.jpg图片分类，输出各图片所属分类的编号、名称。

#### 版本配套表

   | 配套                                                         | 版本    | 环境准备指导                                                 |
   | :------------------------------------------------------------: | :-------: | :------------------------------------------------------------: |
   | 固件与驱动                                                   | 23.0.0   | [固件驱动安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0005.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit) |
   | CANN                                                         | 7.0.0 |[CANN软件包安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0011.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | Python                                                       | 3.9.2   |[Python安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0064.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit#ZH-CN_TOPIC_0000002017916412?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | 硬件设备型号                                                   | Orange Pi AIpro   | -                                                            |

**样例的处理流程如下图所示：**

![输入图片说明](readme_img/zh-cn_image_0000001591372956.png)

### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Samples/ResnetQuickStart
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Samples/ResnetQuickStart
  ```

**样例的代码目录说明如下：**

```
├── data         // 用于存放测试图片
├── model        // 用于存放模型文件
├── script       // 用于存放编译、运行样例的脚本                                              
├── src          // 用于存放源码
```

### 执行准备

1. 设置环境变量。

   ```
   # 设置CANN依赖的基础环境变量
   ./usr/local/Ascend/ascend-toolkit/set_env.sh
   # 配置程序编译依赖的头文件与库文件路径
   export DDK_PATH=/usr/local/Ascend/ascend-toolkit/latest 
   export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub
   ```

### 运行样例

1. 切换到当前样例目录。
2. 获取PyTorch框架的ResNet50模型（\*.onnx），并转换为昇腾AI处理器能识别的模型（\*.om）。
   - 当设备内存**小于8G**时，可设置如下两个环境变量减少atc模型转换过程中使用的进程数，减小内存占用。
     ```
     export TE_PARALLEL_COMPILER=1
     export MAX_COMPILE_CORE_NUMBER=1
     ```
   - 为了方便下载，在这里直接给出原始模型下载及模型转换命令,可以直接拷贝执行。
     ```
     cd model
     wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/003_Atc_Models/resnet50/resnet50.onnx 
     atc --model=resnet50.onnx --framework=5 --output=resnet50 --input_shape="actual_input_1:1,3,224,224"  --soc_version=Ascend310B4
     ```

     atc命令中各参数的解释如下，详细约束说明请参见[《ATC模型转换指南》](https://hiascend.com/document/redirect/CannCommunityAtc)。

     -   --model：ResNet-50网络的模型文件的路径。
     -   --framework：原始框架类型。5表示ONNX。
     -   --output：resnet50.om模型文件的路径。请注意，记录保存该om模型文件的路径，后续开发应用时需要使用。
     -   --input\_shape：模型输入数据的shape。
     -   --soc\_version：昇腾AI处理器的版本。
        >**说明：** 
        >如果无法确定当前设备的soc\_version，则在安装驱动包的服务器执行**npu-smi info**命令进行查询，在查询到的“Name“前增加Ascend信息，例如“Name“对应取值为_xxxyy_，实际配置的soc\_version值为Ascend_xxxyy_。

3. 获取测试图片数据。

   请从以下链接获取该样例的测试图片dog1\_1024\_683.jpg，放在data目录下。

   ```
   cd ../data 
   wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/models/aclsample/dog1_1024_683.jpg
   ```

   **注：**若需更换测试图片，则需自行准备测试图片，并将测试图片放到data目录下。

4. 编译样例源码。

   执行以下命令编译样例源码。

   ```
   cd ../scripts 
   bash sample_build.sh
   ```

5. 运行样例。

   执行以下脚本运行样例：

   ```
   bash sample_run.sh
   ```

   执行成功后，在屏幕上的关键提示信息示例如下，提示信息中的label表示类别标识、conf表示该分类的最大置信度，class表示所属类别。这些 值可能会根据版本、环境有所不同，请以实际情况为准：

    ```
    [INFO] The sample starts to run
    out_dog1_1024_683.jpg
    label:162  conf:0.902209  class:beagle
    [INFO] The program runs successfully
    ```

    图片结果展示：
    ![输入图片说明](https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/models/sampleResnetQuickStart/result.png "out_dog1_1024_683.jpg")


## 代码逻辑详解（C&C++语言）<a name="section1074234113243"></a>

**样例中的接口调用流程如下图所示。**

![输入图片说明](readme_img/zh-cn_image_0000001615415460.png)

**在此样例基础上：**

-   **若想要更换测试图片**，只需自行准备好新的jpg图片并存放到样例的data目录下，图片数据预处理时会自动从该目录下读取图片数据、再缩放至模型所需的大小。
-   **若想要更换模型**，则需关注以下修改点：
    1.  准备模型：需自行准备好原始模型并存放到样例的model目录下，再参考[《ATC模型转换指南》](https://hiascend.com/document/redirect/CannCommunityAtc)转换模型；
    2.  加载模型：在aclmdlLoadFromFile接口处加载转换后的模型；
    3.  准备模型输入/输出数据结构：根据新模型的输入、输出个数准备；
    4.  获取推理结果&后处理：根据新模型的输出数据进行后处理。


>**须知：** 
>一般来说，更换其它图片分类模型（例如resnet50-\>resnet101）,由于同类模型的输入、输出类似，在此样例基础上改动较小，但如果更换为其它类型的模型（例如目标检测模型），由于不同类型模型的输入、输出差别较大，在此样例基础上数据预处理、模型输入&输出准备以及数据后处理等改动很大，建议在[Ascend EdgeAndRobotics仓](https://gitee.com/ascend/EdgeAndRobotics)先找到目标检测类的样例，再基于目标检测样例修改。

## 相关操作<a name="section27901216172515"></a>

-   获取更多样例，请单击[Link](https://gitee.com/ascend/EdgeAndRobotics/tree/master/Samples)。
-   获取在线视频课程，请单击[Link](https://www.hiascend.com/edu/courses?activeTab=%E5%BA%94%E7%94%A8%E5%BC%80%E5%8F%91)。
-   获取学习文档，请单击[AscendCL C&C++](https://hiascend.com/document/redirect/CannCommunityCppAclQuick)或[AscendCL Python](https://hiascend.com/document/redirect/CannCommunityPyaclQuick)，查看最新版本的AscendCL推理应用开发指南。
-   查模型的输入输出

    可使用第三方工具Netron打开网络模型，查看模型输入或输出的数据类型、Shape，便于在分析应用开发场景时使用。
    
    ![输入图片说明](readme_img/zh-cn_image_0000001592270186.png)



