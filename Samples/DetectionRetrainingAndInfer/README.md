# 口罩识别目标检测训练和推理

#### 样例介绍

本样例基于预训练ssd-mobilenet模型使用口罩识别数据集实现了检测口罩佩戴识别的功能，包含训练到om推理全过程。

#### 版本配套表

   | 配套                                                         | 版本    | 环境准备指导                                                 |
   | :------------------------------------------------------------: | :-------: | :------------------------------------------------------------: |
   | 固件与驱动                                                   | 23.0.0   | [固件驱动安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0005.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit) |
   | CANN                                                         | 7.0.0 |[CANN软件包安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0011.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | Python                                                       | 3.9.2   |[Python安装准备](https://www.hiascend.com/document/detail/zh/CANNCommunityEdition/80RC3alpha003/softwareinst/instg/instg_0064.html?Mode=PmIns&OS=Ubuntu&Software=cannToolKit#ZH-CN_TOPIC_0000002017916412?Mode=PmIns&OS=Ubuntu&Software=cannToolKit)|
   | 硬件设备型号                                                   | Orange Pi AIpro   | -                                                            |

#### 样例下载

可以使用以下两种方式下载，请选择其中一种进行源码准备。

- 命令行方式下载（**下载时间较长，但步骤简单**）。

  ```
  # 登录开发板，HwHiAiUser用户命令行中执行以下命令下载源码仓。    
  cd ${HOME}     
  git clone https://gitee.com/ascend/EdgeAndRobotics.git
  # 切换到样例目录
  cd EdgeAndRobotics/Samples/ClassficationRetrainingAndInfer
  ```

- 压缩包方式下载（**下载时间较短，但步骤稍微复杂**）。

  ```
  # 1. 仓右上角选择 【克隆/下载】 下拉框并选择 【下载ZIP】。     
  # 2. 将ZIP包上传到开发板的普通用户家目录中，【例如：${HOME}/EdgeAndRobotics-master.zip】。      
  # 3. 开发环境中，执行以下命令，解压zip包。      
  cd ${HOME} 
  chmod +x EdgeAndRobotics-master.zip
  unzip EdgeAndRobotics-master.zip
  # 4. 切换到样例目录
  cd EdgeAndRobotics-master/Samples/ClassficationRetrainingAndInfer
  ```

#### 执行准备

- 本样例中的模型支持PyTorch2.1.0、torchvision1.16.0版本，请参考[安装PyTorch](https://www.hiascend.com/document/detail/zh/canncommercial/700/envdeployment/instg/instg_0046.html)章节安装PyTorch以及torch_npu插件。
  ```
  # torch_npu由于需要源码编译,速度可能较慢,本样例提供 python3.9,torch2.1版本的torch_npu whl包
  wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/torch_npu-2.1.0rc1-cp39-cp39-linux_aarch64.whl
  
  # 使用pip命令安装
  pip3 install torch_npu-2.1.0rc1-cp39-cp39-linux_aarch64.whl
  ```

- 本样例中的模型还依赖一些其它库（具体依赖哪些库，可查看本样例目录下的requirements.txt文件），可执行以下命令安装：

  ```
  pip3 install -r requirements.txt  # PyTorch2.1版本
  ```

- 配置离线推理所需的环境变量。

  ```
  # 配置程序编译依赖的头文件与库文件路径
  export DDK_PATH=/usr/local/Ascend/ascend-toolkit/latest 
  export NPU_HOST_LIB=$DDK_PATH/runtime/lib64/stub
  ```

- 安装离线推理所需的ACLLite库。

  参考[ACLLite仓](https://gitee.com/ascend/ACLLite)安装ACLLite库。


#### 模型训练

1. 以HwHiAiUser用户登录开发板，切换到样例目录下。
2. 设置环境变量减小算子编译内存占用。
     ```
     export TE_PARALLEL_COMPILER=1
     export MAX_COMPILE_CORE_NUMBER=1
     ```
3. 准备数据集
     ```
     cd dataset
     wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/detection/mask.zip
     unzip mask.zip
     ```


4. 数据集处理,分出训练集和验证集.
   ```
   cd ..
   python3 predata.py
   ```
5. 下载预训练模型
   ```
   cd models
   wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/detection/mobilenet-v1-ssd-mp-0_675.pth
   ```
6. 运行训练脚本。

   ```
   cd ..
   python3 main.py
   ```
   训练完成后，权重文件保存在models目录下，并输出模型训练精度和性能信息。

   此处展示单Device、batch_size=8的训练结果数据：
   |  NAME  | Loss |  FPS  | Epochs | AMP_Type | Torch_Version |
   | :----: | :---: | :---: | :----: | :------: | :-----------: |
   | 1p-NPU | 1.8480 | 2 |   10   |    O2    |      2.1      |


#### 离线推理

1. 以HwHiAiUser用户登录开发板，切换到当前样例目录。
2. 导出onnx模型
   ```
   python3 export.py
   ```

3. 获取测试图片数据。

   ```
   cd omInfer/data
   wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/detection/mask.jpg
   ```

   **注：**若需更换测试图片，则需自行准备测试图片，并将测试图片放到omInfer/data目录下,并修改代码中图片名称。

4. 获取PyTorch框架的mobilenet-ssd模型（\*.onnx），并转换为昇腾AI处理器能识别的模型（\*.om）。
   - 当设备内存**小于8G**时，可设置如下两个环境变量减少atc模型转换过程中使用的进程数，减小内存占用。
     ```
     export TE_PARALLEL_COMPILER=1
     export MAX_COMPILE_CORE_NUMBER=1
     ```
   - 为了方便下载，在这里直接给出原始模型下载及模型转换命令,可以直接拷贝执行。
     ```
     # 将导出的mobilenet-ssd.onnx模型拷贝到model目录下
     cd ../model
     cp ../../mobilenet-ssd.onnx ./
   
     # 获取AIPP配置文件
     wget https://obs-9be7.obs.cn-east-2.myhuaweicloud.com/wanzutao/detection/aipp/aipp.cfg
   
     # 模型转换
     atc --model=mobilenet-ssd.onnx --framework=5 --soc_version=Ascend310B4 --output=mobilenet-ssd --insert_op_conf=aipp.cfg
     ```

     atc命令中各参数的解释如下，详细约束说明请参见[《ATC模型转换指南》](https://hiascend.com/document/redirect/CannCommunityAtc)。

     - --model：转换前模型文件的路径。
     - --framework：原始框架类型。5表示ONNX。
     - --output：转换后模型文件的路径。请注意，记录保存该om模型文件的路径，后续开发应用时需要使用。
     - --input\_shape：模型输入数据的shape。
     - --soc\_version：昇腾AI处理器的版本。


5. 编译样例源码。

   执行以下命令编译样例源码。

   ```
   cd ../scripts 
   bash sample_build.sh
   ```

6. 运行样例。

   执行以下脚本运行样例：

   ```
   bash sample_run.sh
   ```

   执行成功后，omInfer/output目录下会生成检测输出图片

   ![输入图片说明](omInfer/out_0.jpg)


#### 相关操作