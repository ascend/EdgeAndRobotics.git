# Ascend Edge And Robotics

#### 介绍

本仓的样例用于展示开发板外设接口使用方法、开发板上各类AI应用的端到端示例代码，供开发者参考。

本仓支持CANN 7.0.0及以上版本。


```
- /Samples：AI应用开发样例
    |--/ACLHelloWorld：基于AscendCL接口的HelloWorld应用，仅包含运行管理资源申请与释放功能。
	|--/ClassficationRetrainingAndInfer:基于预训练resnet18模型，实现了识别人性别的功能
	|--/DetectionRetrainingAndInfer:基于预训练ssd-mobilenet模型和口罩识别数据集，实现了检测口罩佩戴识别的功能
    |--/HandWritingTrainAndInfer：基于MNIST数据集实现手写数字识别体的训练(Pytorch)到推理全过程。
    |--/ResnetPicture：基于AscendCL接口，使用ResNet-50模型实现图片分类应用。
    |--/ResnetPictureThread：基于AscendCL接口，使用ResNet-50模型采用多线程方式实现图片分类应用。
	|--/ResnetQuickStart:基于AscendCL接口，使用ResNet50模型模型，实现图片分类
    |--/VideoDecode：使用ACLLite接口完成视频解码，读取mp4文件的前十帧并解码为YUV图片。
    |--/VideoEncode：使用ACLLite接口完成视频编码，读取YUV文件的十次并编码为H264视频文件。
	|--/YOLOV5MultiInput:基于YoloV5s模型，对多路离线视频流（*.mp4）中的物体做实时检测，展示推理结果
	|--/YOLOV5USBCamera:基于yolov5s模型，对输入视频中（USB接口连接的Camera中）的物体做实时检测，使用imshow方式显示推理结果
	|--/YOLOV5Video:基于yolov5s模型，对本地输入视频的物体做检测，使用imshow方式显示推理结果
    
- /Peripherals：外设使用指导
    |--/Audio：音频设备
         |--/MIPIAudio：音频从MIPI接口送入，基于AscendCL接口，启用并设置音频设备的相关参数。
         |--/USBAudio：音频从USB接口送入，启用并设置音频设备的相关参数。
    |--/Camera：摄像头
         |--/MIPICamera：视频从MIPI接口的Camera送入，启用Camera设备并解码视频。
         |--/USBCamera：视频从USB接口的Camera送入，启用Camera设备并解码视频。
    |--/Display：显示器
         |--/HDMIDisplay：视频从HDMI接口送入，在显示器中显示。
         |--/MIPIDisplay：视频从MIPI接口送入，在显示器中显示。
```



#### 参与贡献

1. Fork 本仓库
2. 提交代码
3. 新建 Pull Request



#### 修订记录

| 日期   | 更新事项     |
| ------ | ------------ |
| 2024/2/4 | 第一次发布。 |


